package ru.anenkov.tm;

import ru.anenkov.tm.constant.TerminalConst;

public class Application {

    public static void main(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parseArgs(args);
    }

    public static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        if (TerminalConst.HELP.equals(arg)) showHelp();
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Denis Anenkov");
        System.out.println("E-MAIL: denk.an@inbox.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - Show developer info.");
        System.out.println(TerminalConst.VERSION + " - Show version info.");
        System.out.println(TerminalConst.HELP + " - Display terminal commands.");
    }

}
